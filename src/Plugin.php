<?php

declare(strict_types=1);
namespace getdesigned\assetinterceptor;

use Craft;
use craft\base\Element;
use craft\base\Model;
use craft\elements\Asset;
use craft\events\ModelEvent;
use craft\events\RegisterCpAlertsEvent;
use craft\helpers\Cp;
use getdesigned\assetinterceptor\helper\AssetHelper;
use getdesigned\assetinterceptor\models\Settings;
use yii\base\Event;

/**
 * Asset Interceptor plugin.
 *
 * @method Settings getSettings()
 *
 * @since 1.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class Plugin extends \craft\base\Plugin
{
    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();

        Event::on(Cp::class, Cp::EVENT_REGISTER_ALERTS, function(RegisterCpAlertsEvent $event) {
            if (!extension_loaded('exif')) {
                $event->alerts[] = Craft::t(
                    'asset-interceptor',
                    'EXIF metadata of images cannot be extracted: The PHP extension "EXIF" is not loaded.'
                );
            }
        });

        Event::on(Asset::class, Element::EVENT_BEFORE_SAVE, function(ModelEvent $event) {
            if ($event->sender instanceof Asset) {
                AssetHelper::populateMetaData($event->sender);
            }
        });
    }

    /**
     * @inheritDoc
     */
    protected function createSettingsModel()
    {
        return new Settings();
    }
}