<?php

declare(strict_types=1);
namespace getdesigned\assetinterceptor\models;

use craft\base\Model;

/**
 * Settings model.
 *
 * @property-read array $metaDataFieldMappingConfiguration
 *
 * @since 1.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class Settings extends Model
{
    /**
     * This strategy defines that for database fields for which both EXIF and IPTC metadata are available,
     * EXIF is preferred.
     *
     * @since 1.1.0
     */
    const STRATEGY_PREFER_EXIF = 0;

    /**
     * This strategy defines that for database fields for which both EXIF and IPTC metadata are available,
     * IPTC is preferred.
     *
     * @since 1.1.0
     */
    const STRATEGY_PREFER_IPTC = 1;

    /**
     * The identifier for EXIF metadata.
     *
     * @since 1.1.0
     */
    const TYPE_EXIF = 'exif';

    /**
     * The identifier for IPTC metadata.
     *
     * @since 1.1.0
     */
    const TYPE_IPTC = 'iptc';

    /**
     * The configuration for mapping EXIF data to fields.
     *
     * Expects field handles as key and the corresponding EXIF field name(s) – as provided by exif_read_data() – as value(s).
     *
     * ```php
     * return [
     *     'exifFieldMappingConfiguration' => [
     *         'aperture' => 'COMPUTED.ApertureFNumber',
     *         'copyright' => ['COMPUTED.Copyright', 'IFD0.Copyright']
     *     ]
     * ];
     * ```
     *
     * @var array
     */
    public $exifFieldMappingConfiguration = [];

    /**
     * The configuration for mapping IPTC data to fields.
     *
     * Expects field handles as key and the corresponding IPTC field name(s) – as provided by iptcparse() – as value(s).
     *
     * ```php
     * return [
     *     'iptcFieldMappingConfiguration' => [
     *         'copyright' => ['2#116.0']
     *     ]
     * ];
     * ```
     *
     * @since 1.1.0
     * @var array
     */
    public $iptcFieldMappingConfiguration = [];

    /**
     * The strategy to apply when both EXIF and IPTC metadata are available for a single database field.
     *
     * @since 1.1.0
     * @var int
     */
    public $metaDataConflictStrategy = self::STRATEGY_PREFER_EXIF;

    /**
     * Returns the merged metadata field mapping configuration in correct order.
     *
     * @since 1.1.0
     * @return array
     */
    public function getMetaDataFieldMappingConfiguration(): array
    {
        $fieldMappingConfiguration = [];

        if (!empty($this->exifFieldMappingConfiguration)) {
            $fieldMappingConfiguration[self::TYPE_EXIF] = $this->exifFieldMappingConfiguration;
        }

        if (!empty($this->iptcFieldMappingConfiguration)) {
            $fieldMappingConfiguration[self::TYPE_IPTC] = $this->iptcFieldMappingConfiguration;
        }

        if (self::STRATEGY_PREFER_IPTC === $this->metaDataConflictStrategy) {
            $fieldMappingConfiguration = array_reverse($fieldMappingConfiguration);
        }

        return $fieldMappingConfiguration;
    }
}