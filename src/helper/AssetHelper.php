<?php

declare(strict_types=1);
namespace getdesigned\assetinterceptor\helper;

use Craft;
use craft\elements\Asset;
use craft\helpers\ArrayHelper;
use Exception;
use getdesigned\assetinterceptor\models\Settings;
use getdesigned\assetinterceptor\Plugin;
use Throwable;
use yii\base\InvalidConfigException;
use yii\base\UnknownPropertyException;

/**
 * Static helper functions aiding in handling assets.
 *
 * @since 1.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class AssetHelper
{
    /**
     * Extracts the metadata of the given asset and populates its fields according to the metadata field mapping
     * configuration.
     *
     * @since 1.1.0
     * @see \getdesigned\assetinterceptor\models\Settings::$exifFieldMappingConfiguration
     * @see \getdesigned\assetinterceptor\models\Settings::$iptcFieldMappingConfiguration
     * @see \getdesigned\assetinterceptor\models\Settings::$metaDataConflictStrategy
     * @param Asset $asset The asset to modify
     */
    public static function populateMetaData(Asset $asset): void
    {
        if (empty($metaDataConfiguration = Plugin::getInstance()->getSettings()->getMetaDataFieldMappingConfiguration())) {
            return /* void */;
        }

        $fieldValues = [];
        $metaData = self::extractMetaData($asset);

        foreach ($metaDataConfiguration as $type => $fieldMappingConfiguration) {
            foreach ($fieldMappingConfiguration as $fieldName => $candidates) {
                if (empty($candidates) || isset($fieldValues[$fieldName])) {
                    continue;
                }
                foreach ((array)$candidates as $candidate) {
                    try {
                        if (empty($value = ArrayHelper::getValue($metaData[$type], $candidate))) {
                            continue;
                        }
                    } catch (Exception $exception) {continue;}
                    $fieldValues[$fieldName] = $value;
                    break;
                }
            }
        }

        if (!empty($fieldValues)) {
            try {
                $asset->setFieldValues($fieldValues);
            } catch (UnknownPropertyException $exception) {
                Craft::warning($exception->getMessage());
            }
        }
    }

    /**
     * Extracts the EXIF data of the given asset and populates its properties according to the EXIF field mapping
     * configuration.
     *
     * @deprecated since 1.1.0, will be removed with version 1.2.0. Use static::populateMetaData() instead!
     * @see \getdesigned\assetinterceptor\models\Settings::$exifFieldMappingConfiguration
     * @param Asset $asset The asset to modify
     */
    public static function populateExifData(Asset $asset): void
    {
        trigger_deprecation(
            'getdesigned/asset-interceptor',
            '1.1.0',
            __METHOD__ . ' is deprecated and will be removed with version 1.2.0. Please migrate to ' . __CLASS__ . '::populateMetaData().'
        );

        if (
             empty($fieldMappingConfiguration = Plugin::getInstance()->getSettings()->exifFieldMappingConfiguration)
             || !is_array($fieldMappingConfiguration)
             || empty($exif = static::extractExifData($asset))
        ) {
            return;
        }
        foreach ($fieldMappingConfiguration as $fieldName => $candidates) {
            if (empty($candidates)) {
                continue;
            }
            foreach ((array)$candidates as $candidate) {
                if (!empty($value = ArrayHelper::getValue($exif, $candidate))) {
                    try {
                        $asset->setFieldValue($fieldName, $value);
                    } catch (UnknownPropertyException $exception) {
                        Craft::warning('Attempt to populate unknown property "' . $fieldName . '" with EXIF data.');
                    }
                    break;
                }
            }
        }
    }

    /**
     * Extracts the metadata from the given asset.
     *
     * @since 1.1.0
     * @param Asset $asset The asset to extract the metadata from
     * @return array The metadata extracted
     */
    public static function extractMetaData(Asset $asset): array
    {
        if (Asset::KIND_IMAGE !== $asset->kind || empty($path = static::resolvePath($asset))) {
            return [];
        }

        $metaData = [];
        getimagesize($path, $info);

            // EXIF
        if (extension_loaded('exif')) {
            try {
                $metaData[Settings::TYPE_EXIF] = (array)(exif_read_data($path) ?: []);
            } catch (Throwable $exception) {
                $metaData[Settings::TYPE_EXIF] = [];
            }
        }

            // IPTC
        if (!empty($iptc = (string)($info['APP13'] ?? ''))) {
            try {
                $metaData[Settings::TYPE_IPTC] = (array)(iptcparse($iptc) ?: []);
            } catch (Throwable $exception) {
                $metaData[Settings::TYPE_IPTC] = [];
            }
        }

        return $metaData;
    }

    /**
     * Extracts the EXIF data from the given asset.
     *
     * @deprecated since 1.1.0, will be removed with version 1.2.0. Use static::extractMetaData() instead!
     * @param Asset $asset The asset to extract the EXIF data from
     * @return array The EXIF data extracted
     */
    public static function extractExifData(Asset $asset): array
    {
        trigger_deprecation(
            'getdesigned/asset-interceptor',
            '1.1.0',
            __METHOD__ . ' is deprecated and will be removed with version 1.2.0. Please migrate to ' . __CLASS__ . '::extractMetaData().'
        );

        if (
            $asset->kind !== Asset::KIND_IMAGE
            || empty($path = static::resolvePath($asset))
            || empty($exif = (array)(exif_read_data($path) ?: []))
        ) {
            return [];
        }
        return $exif;
    }

    /**
     * Resolves the absolute path for an asset.
     *
     * @param Asset $asset The asset to resolve the path for
     * @return string|null The path resolved, if any
     */
    public static function resolvePath(Asset $asset): ?string
    {
        if (!empty($asset->tempFilePath)) {
            return $asset->tempFilePath;
        }
        try {
            $path = Craft::getAlias($asset->getVolume()->path) . '/' . ltrim($asset->getPath(), '/');
        } catch (InvalidConfigException $exception) {
            return null;
        }
        if (!is_readable($path)) {
            return null;
        }
        return $path;
    }
}