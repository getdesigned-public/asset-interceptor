<?php

/**
 * German translations.
 *
 * @since 1.1.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
return [
    'EXIF metadata of images cannot be extracted: The PHP extension "EXIF" is not loaded.' => 'EXIF-Metadaten von Bildern können nicht extrahiert werden: Die PHP-Erweiterung "EXIF" ist nicht geladen.'
];