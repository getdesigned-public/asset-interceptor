<?php

/**
 * English translations.
 *
 * @since 1.1.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
return [
    'EXIF metadata of images cannot be extracted: The PHP extension "EXIF" is not loaded.' => 'EXIF metadata of images cannot be extracted: The PHP extension "EXIF" is not loaded.'
];